# gp-monitoring



Состав:

- Grafana - web интерфейс. БД postgress, Сессии в redis
- Prometeus - mertics scraper and holder
- Loki - logs stuff
- Traefik - web server


Запуск: 

- переименовать .env_demo в .env и проверить содержимое
- docker-compose up -d
- логинимся по адресу domain.tld, указанному в .env . Логин admin , пароль указать в .енв в gp_GF_SECURITY_ADMIN_PASSWORD
- с главной страницы идем в dashboards, выбираем cadvisor exporter:

![dashboard](img/CADVISORdashboard.png)
- добавляем нужные експортеры и дашборды