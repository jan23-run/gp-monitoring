version: '3.9'

networks:
  monitoring:

volumes:
  grafana:
  loki:
  prometheus:
  postgres:

services:
  mon_db:
    image: postgres:${gp_POSTGRES_VERSION} # no alpine here as it has other data file locations
    container_name: mon_postgres
    restart: unless-stopped
    environment:
    # docker exec -ti postgres psql -U pguser -d grafana
      POSTGRES_PASSWORD: ${gp_POSTGRES_PASSWORD}
      POSTGRES_USER: ${gp_POSTGRES_USER}
      POSTGRES_DB: ${gp_POSTGRES_DB}
      PGDATA: "/var/lib/postgresql/data/pgdata"
    volumes:
      - postgres:/var/lib/postgresql/data
      - ./postgres/postgresql.conf:/var/lib/postgresql/data/postgresql.conf
    shm_size: 512MB
    networks:
      - monitoring
    ports:
      - "127.0.0.1:5432:5432"
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U ${gp_POSTGRES_USER} -d ${gp_POSTGRES_DB}"]
      interval: 10s
      timeout: 5s
      retries: 5
      start_period: 10s

  traefik:
    image: traefik:${gp_TRAEFIK_VERSION}
    container_name: traefik
    restart: unless-stopped
    ports:
      - 80:80
      - 443:443
    volumes:
      - '/var/run/docker.sock:/var/run/docker.sock:ro'
      - /etc/localtime:/etc/localtime:ro
      - ./traefik/traefik.yml:/traefik.yml:ro
      - ./traefik/acme.json:/acme.json
    command:
      - --api.insecure=false
      - --providers.docker
      - --log.level=ERROR
    networks:
      - monitoring

  mon_prometheus:
    image: prom/prometheus:${gp_PROMETHEUS_VERSION}
    container_name: mon_prometheus
    restart: unless-stopped
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
      - ./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
      - prometheus:/prometheus
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
    depends_on:
      - mon_node-exporter
      - mon_cadvisor
      # - mon_loki
      # - mon_promtail
    networks:
      - monitoring

  mon_node-exporter:
    image: prom/node-exporter:${gp_NODE_EXPORTER_VERSION}
    container_name: mon_node-exporter
    restart: unless-stopped
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command:
      - '--path.procfs=/host/proc'
      - '--path.sysfs=/host/sys'
      - '--path.rootfs=/rootfs'
      - '--collector.filesystem.mount-points-exclude="^(/rootfs|/host|)/(sys|proc|dev|host|etc)($$|/)"'
      - '--collector.filesystem.fs-types-exclude="^(sys|proc|auto|cgroup|devpts|ns|au|fuse\.lxc|mqueue)(fs|)$$"'
      - '--collector.disable-defaults'
      - '--collector.cpu'
      - '--collector.cpufreq'
      - '--collector.diskstats'
      - '--collector.meminfo'
      - '--collector.netstat'
    networks:
      - monitoring

  mon_redis:
      image: redis:${gp_REDIS_VERSION}
      container_name: mon_redis
      ports:
      - 6379:6379
      networks:
        - monitoring

  mon_cadvisor:
    image: gcr.io/cadvisor/cadvisor:${gp_CADVISOR_VERSION}
    container_name: mon_cadvisor
    restart: unless-stopped
    ports:
    - 127.0.0.1:8080:8080
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
      - /dev:/dev:ro
    networks:
      - monitoring

  mon_grafana:
    image: grafana/grafana:${gp_GRAFANA_VERSION}
    container_name: mon_grafana
    restart: unless-stopped
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
      - ./grafana/grafana.ini:/etc/grafana/grafana.ini
      - grafana:/var/lib/grafana
    ports:
      - 127.0.0.1:3000:3000
    healthcheck:
      test: ["CMD-SHELL", "curl -f localhost:3000/api/health && echo 'ready'"]
      interval: 10s
      retries: 30
    environment:
      GF_SERVER_DOMAIN: ${gp_GRAFANA_HOSTNAME}
      GF_SERVER_ROOT_URL: https://${gp_GRAFANA_HOSTNAME}:443
      GF_USERS_ALLOW_SIGN_UP: "false"
      GF_SECURITY_ADMIN_PASSWORD: ${gp_GF_SECURITY_ADMIN_PASSWORD}
      GF_SMTP_ENABLED: "false"
    user: "1000"
    labels:
      - 'traefik.enable=true'
      - 'traefik.docker.network=monitoring'
      - 'traefik.http.routers.grafana.rule=Host(`${gp_GRAFANA_HOSTNAME}`)'
      - 'traefik.http.routers.grafana.tls=true'
      - 'traefik.http.routers.grafana.tls.certresolver=letsEncrypt'
      - 'traefik.http.services.grafana.loadBalancer.server.url=http://mon_grafana:3000/'
      - 'traefik.http.services.grafana.loadBalancer.server.port=3000'
    depends_on:
      - mon_prometheus
      - mon_db
      - mon_redis
    networks:
      - monitoring

  # mon_loki:
  #   image: grafana/loki:${gp_LOKI_VERSION}
  #   container_name: mon_loki
  #   restart: unless-stopped
  #   ports:
  #     - 127.0.0.1:3100:3100
  #   command: -config.file=/etc/loki/local-config.yaml
  #   labels:
  #     - "traefik.enable=true"
  #     - "traefik.docker.network=monitoring"
  #     - "traefik.http.middlewares.lokiAuth.basicauth.users=loki:$$apr1$$Gvaflloes$$p.g9hhjQ/u5z7Qlat76EW."
  #     - "traefik.http.routers.loki.entrypoints=loki"
  #     - "traefik.http.routers.loki.rule=PathPrefix(`/loki`)"
  #     - "traefik.http.routers.loki.middlewares=lokiAuth@docker"
  #     - "traefik.http.services.loki.loadBalancer.server.url=http://mon_loki:3100/"
  #     - "traefik.http.services.loki.loadBalancer.server.port=3100"
  #   volumes:
  #     - ./loki/config.yaml:/etc/loki/config.yaml
  #     - loki:/data/loki
  #   networks:
  #     - monitoring

  # mon_promtail:
  #   image: grafana/promtail:latest
  #   container_name: mon_promtail
  #   restart: unless-stopped
  #   volumes:
  #     - /var/log:/var/log
  #     - /var/lib/docker/containers:/var/lib/docker/containers
  #     # - ./promtail:/etc/promtail-config/
  #   command: -config.file=/etc/promtail-config/promtail.yml
  #   networks:
  #     - monitoring
